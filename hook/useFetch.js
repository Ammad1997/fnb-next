import axios from "axios";
import { useState, useEffect } from "react";
import { useOnlineStatus } from "../providers/OnlineStatusProvider";

const useFetch = (prefix) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const isOnline = useOnlineStatus();

  useEffect(() => {
    if (isOnline) {
      axios
        .get(prefix)
        .then((response) => {
          setData(response.data);
          setLoading(false);
        })
        .catch((error) => {
          setError(error);
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  }, [prefix]);

  return { data, error, loading };
};

export default useFetch;

import { EffectCallback, useEffect, useRef } from "react";

export default function useDidMountEffect(fn: EffectCallback) {
  const didMountRef = useRef(true);

  useEffect(() => {
    if (didMountRef.current) {
      return fn();
    }
    didMountRef.current = false;
  }, []);
}

import axios from "axios";
import { AppProps } from "next/app";
import Head from "next/head";

import "../styles/globals.css";

axios.defaults.baseURL = "https://api.emenu.tbrdev.my.id/api/v1";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />
        <title>Food and Beverage</title>

        <link rel="manifest" href="/manifest.json" />
        <link
          href="/icons/icon-48x48.png"
          rel="icon"
          type="image/png"
          sizes="48x48"
        />
        <link
          href="/icons/icon-72x72.png"
          rel="icon"
          type="image/png"
          sizes="72x72"
        />
        <link rel="apple-touch-icon" href="/icons/icon-48x48.png"></link>
        <meta name="theme-color" content="#FFB300" />
      </Head>
      <Component {...pageProps} />
    </>
  );
}

import { useRouter } from "next/router";
import React from "react";

import BaseLayout from "../components/BaseLayout";
import IconBackWhite from "../public/assets/icons/back-white.svg";
import IconConnectionFailed from "../public/assets/icons/error/connection-failed.svg";

export default function ConnectionFailed() {
  const router = useRouter();

  return (
    <BaseLayout>
      <div className="bg-[#ffb300] h-screen">
        <div className="flex h-[70px] items-center">
          <a
            href="#"
            onClick={(event) => {
              event.preventDefault();
              return router.back();
            }}
          >
            <IconBackWhite className="ml-5" />
          </a>
          <h1 className="text-white ml-3 font-semibold">Connection Failed</h1>
        </div>
        <div className="h-[calc(100vh-70px)] bg-white rounded-t-[40px] text-center">
          <div className="h-1/3 flex items-center">
            <div className="px-20">
              <h1 className="mb-2 text-[30px] font-bold text-[#523B5D]">
                Terdapat Kesalahan
              </h1>
              <h6 className="text-[#707793]">
                Sedang terjadi kesalahan pada sistem kami. Secepatnya akan kami
                perbaiki.
              </h6>
            </div>
          </div>
          <div className="h-2/3 bg-[#F8F7F8] rounded-t-[40px] flex justify-center items-center">
            <IconConnectionFailed />
          </div>
        </div>
      </div>
    </BaseLayout>
  );
}

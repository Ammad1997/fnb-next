import BaseLayout from "../components/BaseLayout";

const Home = () => {
  return (
    <BaseLayout>
      <div className="h-screen bg-gradient-to-b from-[#ffb300] to-[#ffc232]">
        <div className="flex h-[50vh] items-center justify-center bg-[url('/assets/bg/top.png')] bg-contain bg-no-repeat">
          <div className="w-2/3 text-center">
            <h1 className="mb-[15px] text-[40px] font-bold text-[#523B5D]">
              Coming Soon
            </h1>
            <p className="text-[18px] font-semibold text-white">
              Saat ini masih dalam tahap pengembangan. Stay tuned ya ...
            </p>
          </div>
        </div>
        <div className="h-[50vh] rounded-t-[40px] bg-[url('/assets/bg/bottom.jpg')] bg-cover"></div>
      </div>
    </BaseLayout>
  );
};

export default Home;

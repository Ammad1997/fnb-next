import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import InfiniteScroll from "react-infinite-scroller";

import Bottomnavigation from "../../components/BottomNavigation";
import ContactFloatingButton from "../../components/ContactFloatingButton";
import Menucard from "../../components/MenuCard";
import MenuCategory from "../../components/MenuCategory";
import MenuNotFound from "../../components/MenuNotFound";
import ReadMore from "../../components/ReadMore";
import Restaurantlayout from "../../components/RestaurantLayout";
import useDidUpdateEffect from "../../hook/useDidUpdateEffect";
import useWindowPosition from "../../hook/useWindowPosition";
import NoImageRestaurant from "../../public/assets/icons/no-image-restaurant.svg";

export default function Username({ restaurant, menus: initialMenus }: any) {
  const categoryRef = useRef();
  const [activeCategory, setActiveCategory] = useState("all");
  const [categoryStyle, setCategoryStyle] = useState(
    "m-[20px] flex overflow-hidden",
  );
  const [categoryPosition, setCategoryPosition] = useState(361);
  const windowPosition = useWindowPosition();
  const [nextPageUrl, setNextPageUrl] = useState(initialMenus.next_page_url);
  const hasMoreMenus = nextPageUrl ? true : false;
  const [menus, setMenus] = useState(initialMenus.data);

  useEffect(() => {
    if (windowPosition >= categoryPosition) {
      setCategoryStyle(
        "p-[20px] fixed top-0 w-[480px] bg-white overflow-hidden transition-[top] duration-300 z-50",
      );
    } else {
      setCategoryStyle(
        "p-[20px] fixed top-[-100px] w-[480px] bg-white overflow-hidden transition-[top] duration-300 z-50",
      );
    }
  }, [windowPosition]);

  useDidUpdateEffect(() => {
    axios(
      `/restaurant/${restaurant.username}/restaurant-menu?category=${activeCategory}`,
    ).then((res) => {
      setMenus(res.data.data.restaurant_menus.data);
      setNextPageUrl(res.data.data.restaurant_menus.next_page_url);
    });
  }, [activeCategory]);

  function handleLoadMore() {
    if (nextPageUrl) {
      fetch(nextPageUrl)
        .then((res) => res.json())
        .then((data) => {
          setMenus([...menus, ...data.data.restaurant_menus.data]);
          setNextPageUrl(data.data.restaurant_menus.next_page_url);
        });
    }
  }

  return (
    <Restaurantlayout>
      <div className="rounded-b-[40px] bg-white">
        <div
          className="flex aspect-[270/127] items-center bg-[#ffb300] bg-cover rounded-b-[40px]"
          style={{
            backgroundImage: restaurant.banner_path
              ? `url(${restaurant.banner_path})`
              : `url(https://cdn.lorem.space/images/pizza/.cache/1080x508/pexels-daria-shevtsova-1260968.jpg)`,
          }}
        ></div>
        <div className="flex m-[20px] pb-[20px]">
          {restaurant.full_photo_path ? (
            <img
              className="h-24 w-24 mr-[20px] flex-shrink-0 rounded-[50%] shadow-[0px_0px_50px_30px_rgba(255,179,0,0.1)]"
              src={restaurant.full_photo_path}
            />
          ) : (
            <>
              <div className="flex h-24 w-24 mr-[20px] flex-shrink-0 items-center justify-center rounded-[50%] bg-white shadow-[0px_40px_50px_-10px_rgba(255,179,0,0.1)]">
                <NoImageRestaurant />
              </div>
            </>
          )}
          <div className="block">
            <h1 className="mb-2 text-lg font-bold text-[#523B5D]">
              {restaurant.name}
            </h1>
            <ReadMore
              onOpen={() => {
                setCategoryPosition(categoryRef.current.offsetTop);
              }}
              text={restaurant.description}
              maxLength={50}
            />
          </div>
        </div>
      </div>
      <MenuCategory
        className="m-[20px] flex overflow-hidden"
        categoryRef={categoryRef}
        setActiveCategory={setActiveCategory}
        categories={restaurant.categories}
        activeCategory={activeCategory}
      />
      <MenuCategory
        className={categoryStyle}
        setActiveCategory={setActiveCategory}
        categories={restaurant.categories}
        activeCategory={activeCategory}
        categoryRef={undefined}
      />
      <div className="mb-[100px]">
        <InfiniteScroll loadMore={handleLoadMore} hasMore={hasMoreMenus}>
          {menus.length ? (
            menus.map((menu: any, index: number) => {
              return (
                <Menucard
                  key={index}
                  menu={menu}
                  username={restaurant.username}
                />
              );
            })
          ) : (
            <MenuNotFound />
          )}
        </InfiniteScroll>
      </div>
      <Bottomnavigation username={restaurant.username} />
      <ContactFloatingButton restaurant={restaurant} />
    </Restaurantlayout>
  );
}

export async function getServerSideProps({ params }: any) {
  const restaurant = await axios(`/restaurant/${params.username}`).then(
    (res: any) => res.data.data.restaurant,
  );
  const menus = await axios(
    `/restaurant/${params.username}/restaurant-menu`,
  ).then((res: any) => {
    return res.data.data.restaurant_menus;
  });

  return {
    props: {
      restaurant,
      menus,
    },
  };
}

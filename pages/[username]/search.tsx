import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import InfiniteScroll from "react-infinite-scroller";

import Bottomnavigation from "../../components/BottomNavigation";
import Menucard from "../../components/MenuCard";
import MenuNotFound from "../../components/MenuNotFound";
import Restaurantlayout from "../../components/RestaurantLayout";
import useDebounce from "../../hook/useDebounce";
import useDidMountEffect from "../../hook/useDidMountEffect";
import useDidUpdateEffect from "../../hook/useDidUpdateEffect";
import IconBack from "../../public/assets/icons/back.svg";

export default function Search({ username, menus: initialMenus }: any) {
  const search = useRouter();
  const [searchQuery, setSearchQuery] = useState(search.query.search);
  const [nextPageUrl, setNextPageUrl] = useState(initialMenus.next_page_url);
  const [hasMoreMenus, setHasMoreMenus] = useState(false);
  const [menus, setMenus] = useState(initialMenus.data);
  const debounceSearchQuery = useDebounce(searchQuery, 500);

  function handleSearch(value: any) {
    axios(`/restaurant/${username}/restaurant-menu?search=${value}`).then(
      (res: any) => {
        if (res.data.data.restaurant_menus.next_page_url !== null) {
          setHasMoreMenus(true);
        } else {
          setHasMoreMenus(false);
        }

        setMenus(res.data.data.restaurant_menus.data);
        setNextPageUrl(res.data.data.restaurant_menus.next_page_url);
      },
    );
  }

  function handleLoadMore() {
    if (nextPageUrl) {
      axios(nextPageUrl).then((res) => {
        if (res.data.data.restaurant_menus.next_page_url !== null) {
          setHasMoreMenus(true);
        } else {
          setHasMoreMenus(false);
        }

        setMenus([...menus, ...res.data.data.restaurant_menus.data]);
        setNextPageUrl(res.data.data.restaurant_menus.next_page_url);
      });
    }
  }

  useDidUpdateEffect(() => {
    handleSearch(debounceSearchQuery);
  }, [debounceSearchQuery]);

  useDidMountEffect(() => {
    if (nextPageUrl !== null) {
      setHasMoreMenus(true);
    } else {
      setHasMoreMenus(false);
    }
  });

  return (
    <Restaurantlayout>
      <div className="fixed w-full sm:w-[480px]">
        <div className="d-block flex h-20 items-center gap-2 bg-white px-4">
          <div className="flex w-10 justify-center">
            <Link href={"/" + username}>
              <a>
                <IconBack />
              </a>
            </Link>
          </div>
          <div className="w-full">
            <input
              className="border-3 h-12 w-full rounded-lg border-[3px] border-[#ffb300] p-2 focus:outline-none"
              type="text"
              placeholder="Search"
              defaultValue={searchQuery}
              onChange={(event) => {
                setSearchQuery(event.target.value);
              }}
            />
          </div>
        </div>
      </div>
      <div className="my-[100px]">
        <InfiniteScroll loadMore={handleLoadMore} hasMore={hasMoreMenus}>
          {menus.length ? (
            menus.map((menu: any, index: number) => {
              return <Menucard key={index} menu={menu} username={username} />;
            })
          ) : (
            <MenuNotFound />
          )}
        </InfiniteScroll>
      </div>
      <Bottomnavigation username={username} />
    </Restaurantlayout>
  );
}

export async function getServerSideProps({ params }: any) {
  const menus = await axios(
    `/restaurant/${params.username}/restaurant-menu`,
  ).then((res: any) => {
    return res.data.data.restaurant_menus;
  });

  return {
    props: {
      username: params.username,
      menus,
    },
  };
}

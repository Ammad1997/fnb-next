import axios from "axios";
import { useRouter } from "next/router";
import React from "react";

import Bottomnavigation from "../../../components/BottomNavigation";
import Restaurantlayout from "../../../components/RestaurantLayout";
import converToRupiah from "../../../helper/convertToRupiah";

export default function Menu({ menu, username }: any) {
  const router = useRouter();

  return (
    <Restaurantlayout>
      <div
        className="flex h-screen flex-col justify-end bg-[#ffb300] bg-[length:auto_480px] bg-top bg-no-repeat"
        style={{ backgroundImage: `url(${menu.full_photo_path})` }}
      >
        <div className="h-[65vh] rounded-t-3xl bg-white p-10">
          <div className="h-[65%] overflow-y-auto mb-5 relative">
            {menu.is_sold_out ? (
              <div className="bg-red-600 absolute text-white px-5 py-2 rounded-[10px] top-0 right-0">
                Habis
              </div>
            ) : (
              ""
            )}
            <h1 className="mb-2 text-3xl font-bold text-[#390606]">
              {menu.name}
            </h1>
            <div className="flex">
              {menu.categories &&
                menu.categories.map((category: any, index: number) => {
                  return (
                    <div
                      key={index}
                      className="mb-5 h-[30px] px-[12px] flex items-center rounded-[20px] text-white bg-[#ffb300] mr-2"
                    >
                      {category.name}
                    </div>
                  );
                })}
            </div>
            <h3 className="mb-5 text-2xl font-bold text-[#fb9300]">
              {converToRupiah(menu.price)}
            </h3>
            <div dangerouslySetInnerHTML={{ __html: menu.description }} />
          </div>
          <button
            onClick={() => router.back()}
            className="w-full rounded-lg bg-[#fff7e5] px-5 py-3 font-semibold text-[#ffb300]"
          >
            Kembali
          </button>
        </div>
      </div>
      <Bottomnavigation username={username} />
    </Restaurantlayout>
  );
}

export async function getServerSideProps({ params }: any) {
  const menu = await axios(
    `/restaurant/${params.username}/restaurant-menu/${params.id}`,
  ).then((res: any) => {
    return res.data.data.restaurant_menu;
  });

  return {
    props: {
      menu,
      username: params.username,
    },
  };
}

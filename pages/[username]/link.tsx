import axios from "axios";

import Bottomnavigation from "../../components/BottomNavigation";
import ReadMore from "../../components/ReadMore";
import Restaurantlayout from "../../components/RestaurantLayout";
import NoImageRestaurant from "../../public/assets/icons/no-image-restaurant.svg";

export default function link({ links, restaurant }: any) {
  return (
    <Restaurantlayout>
      <div className="rounded-b-3xl bg-white pb-1">
        <div className="mb-[20px] h-5 rounded-b-3xl bg-[#ffb300]"></div>
        <div className="flex m-[20px]">
          {restaurant.full_photo_path ? (
            <img
              className="h-24 w-24 mr-[20px] flex-shrink-0 rounded-[50%] shadow-[0px_0px_50px_30px_rgba(255,179,0,0.1)]"
              src={restaurant.full_photo_path}
            />
          ) : (
            <>
              <div className="flex h-24 w-24 mr-[20px] flex-shrink-0 items-center justify-center rounded-[50%] bg-white shadow-[0px_40px_50px_-10px_rgba(255,179,0,0.1)]">
                <NoImageRestaurant />
              </div>
            </>
          )}
          <div className="block">
            <h1 className="mb-2 text-lg font-bold text-[#523B5D]">
              {restaurant.name}
            </h1>
            <ReadMore text={restaurant.description} maxLength={50} />
          </div>
        </div>
      </div>
      <div className="mb-[55px] min-h-[50vh] p-5">
        {links.length ? (
          links.map((item: any) => {
            return (
              <a
                href={item.url}
                target="_blank"
                rel="noreferrer"
                className="mb-3 flex items-center overflow-hidden rounded-lg bg-white"
              >
                <div
                  className="mr-[40px] flex h-12 w-12 items-center justify-center"
                  style={{ backgroundColor: item.bg_color }}
                >
                  <img src={item.icon} alt="" />
                </div>
                <p className="font-bold text-[#523B5D]">{item.name}</p>
              </a>
            );
          })
        ) : (
          <div className="flex h-40 items-center justify-center">
            <p className="text-center text-[#707793]">
              This restaurant doesn't have any links yet.
            </p>
          </div>
        )}
      </div>
      <Bottomnavigation username={restaurant.username} />
    </Restaurantlayout>
  );
}

export async function getServerSideProps({ params }: any) {
  const { username } = params;
  const restaurant = await axios(`/restaurant/${params.username}`).then(
    (res: any) => res.data.data.restaurant,
  );
  const links = await axios(`/restaurant/${username}/link`).then((res: any) => {
    return res.data.data.links;
  });

  return {
    props: {
      restaurant,
      links,
    },
  };
}

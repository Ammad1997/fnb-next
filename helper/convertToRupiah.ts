export default function converToRupiah(amount: number, prefix: boolean = true) {
  let numberString = amount.toString(),
    sisa = numberString.length % 3,
    rupiah = numberString.substr(0, sisa),
    ribuan = numberString.substr(sisa).match(/\d{3}/g);

  if (ribuan) {
    let separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }

  rupiah = prefix ? "Rp. " + rupiah : rupiah;
  return rupiah;
}

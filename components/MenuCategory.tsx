import React from "react";
import "swiper/css";
import { Swiper, SwiperSlide } from "swiper/react";

export default function MenuCategory({
  className,
  categoryRef,
  setActiveCategory,
  categories,
  activeCategory,
}): any {
  const activeCategoryClass = "text-white bg-[#ffb300]";
  const inactiveCategoryClass = "text-[#ffb300] bg-[rgb(255,179,0,0.1)]";

  return (
    <div className={className} ref={categoryRef}>
      <Swiper slidesPerView={"auto"} spaceBetween={10} className="!m-0">
        <SwiperSlide
          key="0"
          style={{
            width: "auto",
          }}
          onClick={() => setActiveCategory("all")}
        >
          <button
            className={`w-full rounded-[10px] px-[12px] h-[38px] flex items-center ${
              activeCategory === null || activeCategory === "all"
                ? activeCategoryClass
                : inactiveCategoryClass
            } truncate`}
          >
            Semua
          </button>
        </SwiperSlide>
        {categories.map((category: any) => (
          <SwiperSlide
            key={category.id}
            style={{
              width: "auto",
            }}
            onClick={() => setActiveCategory(category.name)}
          >
            <button
              className={`w-full rounded-[10px] px-[12px] h-[38px] flex items-center ${
                activeCategory === category.name
                  ? activeCategoryClass
                  : inactiveCategoryClass
              } truncate`}
            >
              {category.name}
            </button>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}

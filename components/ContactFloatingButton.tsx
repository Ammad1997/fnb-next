import React, { useEffect, useState } from "react";

import IconClose from "../public/assets/icons/contact/close.svg";
import IconEmail from "../public/assets/icons/contact/email.svg";
import IconOption from "../public/assets/icons/contact/option.svg";
import IconPhone from "../public/assets/icons/contact/phone.svg";
import IconWhatsapp from "../public/assets/icons/contact/whatsapp.svg";

export default function ContactFloatingButton({ restaurant }: any) {
  const openMenuClass = "w-[162px] right-[20px] bottom-[80px] p-[20px]";
  const closedMenuClass = "w-1 h-1 right-[50px] bottom-[50px] p-0";
  const [size, setSize] = useState(closedMenuClass);
  const [optionClassname, setOptionClassname] = useState("");
  const [closedClassname, setClosedClassname] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  function getFirstParentMatch(el: any, selector: any) {
    const matchesSelector =
      el.matches ||
      el.webkitMatchesSelector ||
      el.mozMatchesSelector ||
      el.msMatchesSelector;
    while (el) {
      if (matchesSelector.call(el, selector)) {
        return el;
      }
      el = el.parentElement;
    }
    return null;
  }

  useEffect(() => {
    if (isOpen) {
      setSize(openMenuClass);
      setClosedClassname("opacity-1 rotate-0");
      setOptionClassname("opacity-0 rotate-180");
    } else {
      setSize(closedMenuClass);
      setClosedClassname("opacity-0 rotate-180");
      setOptionClassname("opacity-1 rotate-0");
    }
    document.addEventListener("mousedown", function (event: any) {
      if (
        event.target.id !== "contact-floating-button" &&
        getFirstParentMatch(event.target, "#floating-button-item") === null
      ) {
        setIsOpen(false);
      }
    });
  }, [isOpen]);

  return (
    <div className="absolute right-[20px] top-0 h-16 w-16">
      <div className="fixed bottom-[100px] inline-flex justify-end">
        <button
          onClick={() => {
            if (size === closedMenuClass) {
              setIsOpen(true);
            } else {
              setIsOpen(false);
            }
          }}
          className="h-16 w-16 rounded-[50%] bg-[#ffb300] shadow-[0px_0px_50px_30px_rgba(255,179,0,0.1)] relative"
        >
          <IconOption
            className={
              "absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition-all " +
              optionClassname
            }
          />
          <IconClose
            className={
              "absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition-all " +
              closedClassname
            }
          />
          <div
            id="contact-floating-button"
            className="absolute top-1/2 -translate-x-1/2 -translate-y-1/2 left-1/2 bg-transparent h-16 w-16 rounded-[50%]"
          ></div>
        </button>
        <div className="relative" id="floating-button-item">
          <div
            className={`${size} bg-white rounded-[12px] absolute overflow-hidden transition-all z-[-1] shadow-[0px_10px_50px_10px_rgba(255,179,0,0.1)] flex flex-col justify-between`}
          >
            {restaurant.map && (
              <a href={restaurant.map} className="flex mb-[20px]">
                <IconEmail className="mr-[12px]" />
                <h4 className="text-[#523B5D]">Map</h4>
              </a>
            )}
            {restaurant.email && (
              <a
                href={`mailto:${restaurant.email}`}
                className={`flex ${
                  restaurant.phone || restaurant.whatsapp ? "mb-[20px]" : ""
                }`}
              >
                <IconEmail className="mr-[12px]" />
                <h4 className="text-[#523B5D]">Email</h4>
              </a>
            )}
            {restaurant.phone && (
              <a
                href={`${`tel:${restaurant.phone}`}`}
                className="flex mb-[20px]"
              >
                <IconPhone className="mr-[12px]" />
                <h4 className="text-[#523B5D]">Phone</h4>
              </a>
            )}
            {restaurant.whatsapp && (
              <a
                href={`${`https://api.whatsapp.com/send?phone=${restaurant.whatsapp}`}`}
                className="flex"
              >
                <IconWhatsapp className="mr-[12px]" />
                <h4 className="text-[#523B5D]">Whatsapp</h4>
              </a>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

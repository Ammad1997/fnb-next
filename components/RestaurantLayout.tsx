import LayoutProps from "../interfaces/LayoutProps";
import BaseLayout from "./BaseLayout";

const Restaurantlayout = ({ children }: LayoutProps) => {
  return <BaseLayout>{children}</BaseLayout>;
};

export default Restaurantlayout;

import React, { useEffect, useState } from "react";

const ReadMore = ({
  text,
  maxLength,
  readMoreText = "more",
  onOpen,
}: {
  text: string;
  maxLength: number;
  readMoreText?: string;
  onOpen?: () => void;
}) => {
  const [readMoreValue, setReadMoreValue] = useState(
    sliceText(text, maxLength),
  );
  const [showReadMoreLink, setShowReadMoreLink] = useState(true);

  function sliceText(text: string, maxLength: number) {
    if (text.length > maxLength) {
      return text.slice(0, maxLength) + " ...";
    }

    return text;
  }

  useEffect(() => {
    onOpen && onOpen();
  }, [showReadMoreLink]);

  useState(() => {
    if (text.length > maxLength) {
      setShowReadMoreLink(true);
    } else {
      setShowReadMoreLink(false);
    }
  }, [text]);

  return (
    <>
      <p className="text-[#707793] break-all">
        {readMoreValue}{" "}
        {showReadMoreLink && (
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              setReadMoreValue(text);
              setShowReadMoreLink(false);
            }}
            className="text-[#ffb300]"
          >
            {readMoreText}
          </a>
        )}
      </p>
    </>
  );
};

export default ReadMore;

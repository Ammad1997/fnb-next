import React from "react";

import EmptyIcon from "../public/assets/icons/Empty.svg";

const MenuNotFound = () => {
  return (
    <div className="flex flex-col items-center">
      <EmptyIcon className="my-6" />
      <span className=" text-gray-600">Saat ini menu belum tersedia</span>
    </div>
  );
};

export default MenuNotFound;

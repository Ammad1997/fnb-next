import LayoutProps from "../interfaces/LayoutProps";

export default function BaseLayout({ children }: LayoutProps) {
  return (
    <div className="flex justify-center">
      <div
        className="w-full sm:w-[480px] min-h-screen relative"
        style={{
          backgroundColor: "#FAF9FB",
        }}
      >
        {children}
      </div>
    </div>
  );
}

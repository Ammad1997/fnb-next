import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const Bottomnavigation = ({ username }: { username: string }) => {
  const location = useRouter().pathname;
  const segments = location.split("/");
  const restaurantUrl = segments[0] + "/" + username;
  let menuSegment = segments[2] || "home";
  const [active, setActive] = useState(menuSegment);
  const childStyle = {
    width: "60px",
    display: "flex",
    justifyContent: "center",
  };

  useEffect(() => {
    setActive(menuSegment);
  }, [location]);

  return (
    <div className="fixed bottom-0 flex w-full justify-evenly rounded-t-[16px] bg-white py-2 sm:w-[480px]">
      <div style={childStyle}>
        <Link href={restaurantUrl}>
          <a
            className="flex h-[60px] w-[60px] items-center justify-center"
            onClick={() => setActive("home")}
          >
            <svg
              id="Layer_2"
              data-name="Layer 2"
              xmlns="http://www.w3.org/2000/svg"
              width="30"
              height="30"
              viewBox="0 0 24 24"
            >
              <g id="home">
                <rect
                  id="Rectangle_5"
                  data-name="Rectangle 5"
                  width="24"
                  height="24"
                  fill="#707793"
                  opacity="0"
                />
                <path
                  id="Path_3"
                  data-name="Path 3"
                  d="M20.42,10.18,12.71,2.3a1,1,0,0,0-1.42,0L3.58,10.19A2,2,0,0,0,3,11.62V20a2,2,0,0,0,1.89,2H19.11A2,2,0,0,0,21,20V11.62a2.07,2.07,0,0,0-.58-1.44ZM10,20V14h4v6Zm9,0H16V13a1,1,0,0,0-1-1H9a1,1,0,0,0-1,1v7H5V11.58l7-7.15,7,7.19Z"
                  transform="translate(0 -0.002)"
                  fill={active === "home" ? "#ffb300" : "#707793"}
                />
              </g>
            </svg>
          </a>
        </Link>
      </div>
      <div style={childStyle}>
        <Link href={restaurantUrl + "/search"}>
          <a
            className="flex h-[60px] w-[60px] items-center justify-center"
            onClick={() => setActive("search")}
          >
            <svg
              id="Layer_2"
              data-name="Layer 2"
              xmlns="http://www.w3.org/2000/svg"
              width="30"
              height="30"
              viewBox="0 0 24 24"
            >
              <g id="search">
                <rect
                  id="Rectangle_4"
                  data-name="Rectangle 4"
                  width="24"
                  height="24"
                  fill="#707793"
                  opacity="0"
                />
                <path
                  id="Path_2"
                  data-name="Path 2"
                  d="M20.71,19.29l-3.4-3.39A7.92,7.92,0,0,0,19,11a8,8,0,1,0-8,8,7.92,7.92,0,0,0,4.9-1.69l3.39,3.4a1,1,0,1,0,1.42-1.42ZM5,11a6,6,0,1,1,6,6,6,6,0,0,1-6-6Z"
                  fill={active === "search" ? "#ffb300" : "#707793"}
                />
              </g>
            </svg>
          </a>
        </Link>
      </div>
      <div style={childStyle}>
        <Link href={restaurantUrl + "/link"}>
          <a
            className="flex h-[60px] w-[60px] items-center justify-center"
            onClick={() => setActive("link")}
          >
            <svg
              id="Layer_2"
              data-name="Layer 2"
              xmlns="http://www.w3.org/2000/svg"
              width="30"
              height="30"
              viewBox="0 0 24 24"
            >
              <g id="link-2">
                <rect
                  id="Rectangle_8"
                  data-name="Rectangle 8"
                  width="24"
                  height="24"
                  fill="#707793"
                  opacity="0"
                />
                <path
                  id="Path_6"
                  data-name="Path 6"
                  d="M13.29,9.29l-4,4a1,1,0,1,0,1.42,1.42l4-4a1,1,0,0,0-1.42-1.42Z"
                  fill={active === "link" ? "#ffb300" : "#707793"}
                />
                <path
                  id="Path_7"
                  data-name="Path 7"
                  d="M12.28,17.4,11,18.67a4.2,4.2,0,0,1-5.58.4,4,4,0,0,1-.27-5.93l1.42-1.43a1,1,0,1,0-1.42-1.42L3.88,11.57a6.15,6.15,0,0,0-.67,8.07,6.06,6.06,0,0,0,9.07.6l1.42-1.42a1,1,0,1,0-1.42-1.42Z"
                  fill={active === "link" ? "#ffb300" : "#707793"}
                />
                <path
                  id="Path_8"
                  data-name="Path 8"
                  d="M19.66,3.22a6.18,6.18,0,0,0-8.13.68L10.45,5a1.09,1.09,0,0,0-.17,1.61,1,1,0,0,0,1.42,0L13,5.3a4.17,4.17,0,0,1,5.57-.4,4,4,0,0,1,.27,5.95l-1.42,1.43a1,1,0,1,0,1.42,1.42l1.42-1.42a6.06,6.06,0,0,0-.6-9.06Z"
                  fill={active === "link" ? "#ffb300" : "#707793"}
                />
              </g>
            </svg>
          </a>
        </Link>
      </div>
      <div style={childStyle}>
        <Link href={restaurantUrl + "/share"}>
          <a
            className="flex h-[60px] w-[60px] items-center justify-center"
            onClick={() => setActive("share")}
          >
            <svg
              id="Layer_2"
              data-name="Layer 2"
              xmlns="http://www.w3.org/2000/svg"
              width="30"
              height="30"
              viewBox="0 0 24 24"
            >
              <g id="share">
                <rect
                  id="Rectangle_6"
                  data-name="Rectangle 6"
                  width="24"
                  height="24"
                  fill="#707793"
                  opacity="0"
                />
                <path
                  id="Path_4"
                  data-name="Path 4"
                  d="M18,15a3,3,0,0,0-2.1.86L8,12.34v-.67l7.9-3.53A3,3,0,1,0,15,6v.34L7.1,9.86a3,3,0,1,0,0,4.28L15,17.67V18a3,3,0,1,0,3-3ZM18,5a1,1,0,1,1-1,1A1,1,0,0,1,18,5ZM5,13a1,1,0,1,1,1-1A1,1,0,0,1,5,13Zm13,6a1,1,0,1,1,1-1A1,1,0,0,1,18,19Z"
                  fill={active === "share" ? "#ffb300" : "#707793"}
                />
              </g>
            </svg>
          </a>
        </Link>
      </div>
    </div>
  );
};

export default Bottomnavigation;

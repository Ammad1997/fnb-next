import Link from "next/link";
import React from "react";

import converToRupiah from "../helper/convertToRupiah";
import NoImageBuffet from "../public/assets/icons/no-image-buffet.svg";

const Menucard = ({ menu, username }: any) => {
  const url = `/${username}/menu/${menu.id}`;

  return (
    <div>
      <Link href={url}>
        <a className="mx-4 mb-2 flex h-24 rounded-[20px] bg-white">
          {menu.image ? (
            <img
              className="h-100 w-32 rounded-l-[20px] object-cover"
              src={menu.image}
              alt="image buffet"
            />
          ) : (
            <div className="h-100 flex w-32 items-center justify-center rounded-l-[20px] bg-[#ffb300]">
              <NoImageBuffet alt="No image buffet" />
            </div>
          )}
          <div className="flex w-full flex-col justify-center p-4 leading-normal relative">
            <p className="mb-2 text-[16px] font-extrabold text-[#523B5D]">
              {menu.name}
            </p>
            <p className="text-[16px] font-extrabold text-[#fb9300]">
              {converToRupiah(menu.price)}
            </p>
            {menu.is_sold_out ? (
              <div className="bg-red-600 absolute text-white py-1 px-3 rounded-[10px] top-4 right-4 text-sm">
                Habis
              </div>
            ) : (
              ""
            )}
          </div>
        </a>
      </Link>
    </div>
  );
};

export default Menucard;
